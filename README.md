# DenvosVideoMaker

此项目正在开发中...

它可以把视频所有的帧全部提取出来，并保存为图片。
同时也支持把图片以帧的形式合成视频。
UI支持6中主题切换。

仅支持Windows。

软件截图：
![screenshot](https://gitlab.com/Denvo/denvosvideomaker/-/raw/main/resources/1.png)
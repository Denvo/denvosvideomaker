package UI;

import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatIntelliJLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.themes.FlatMacDarkLaf;
import com.formdev.flatlaf.themes.FlatMacLightLaf;

import javax.swing.*;
import java.awt.*;

/**
 * 对UI操作的工具类.
 * @author Denvo Zonis
 * @version 0.0.1
 */

public class UtilsUI {
    /*
    设定的两个主题(亮&暗).
    主题全部来自依赖项:flatlaf.
     */
    public final static LookAndFeel light = new FlatLightLaf();
    public final static LookAndFeel dark = new FlatDarkLaf();
    public final static LookAndFeel intelliJ = new FlatIntelliJLaf();
    public final static LookAndFeel darcula = new FlatDarculaLaf();
    public final static LookAndFeel macLight = new FlatMacLightLaf();
    public final static LookAndFeel macDark = new FlatMacDarkLaf();
    /**
     * 更换主题.
     * @param newLook 新的主题.从类中的静态变量获取.
     * @param c 把主题更新在哪个组件上.
     */
    public static void changeLook(LookAndFeel newLook,Component c) {
        try {UIManager.setLookAndFeel(newLook);
            SwingUtilities.updateComponentTreeUI(c);
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }
    /**
     * 将组件全部添加到容器中的方法.
     * @param main 添加到的容器.
     * @param needAddComponents 需要添加的组件.
     */
    public static <T extends JComponent> void addComponents(T main,Component... needAddComponents) {
        for (Component component : needAddComponents) {
            main.add(component);
        }
    }
    public static <T extends Container> void addComponents(T main, Component... needAddComponents) {
        for (Component component : needAddComponents) {
            main.add(component);
        }
    }
    public static <T extends ButtonGroup> void addButtons(T main, AbstractButton... needAddButtons) {
        for (AbstractButton button : needAddButtons) {
            main.add(button);
        }
    }
}
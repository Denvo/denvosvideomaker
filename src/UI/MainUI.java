package UI;

import main.CV;

import javax.swing.*;
import java.awt.*;
import java.io.File;

import static UI.UtilsUI.*;

public class MainUI {
    private final Image icon = new ImageIcon(ClassLoader.getSystemResource("resources/mainIcon.png")).getImage();
    //用来储存主题设置的.设置请见themeSetting()方法.至于为什么这么设计,还是因为lambda只允许(有效)final变量.
    private final int[] themeModeSet = {0};
    //储存模式设置,见modeChoosePanel()方法.
    private int modeSet = 0;
    //选择的文件或文件夹.
    private File selectedFile;
    private String outPath;
    private String selectedExtension = "png";
    private String selectedName = "新的视频";
    private int selectedFps = 24;
    public MainUI() {
        //JFrame基本设置.
        JFrame jf = new JFrame("DenvosVideoMaker");
        jf.setSize(800,250);
        jf.setResizable(false);
        jf.setLocationRelativeTo(null);
        jf.setIconImage(icon);
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        changeLook(light,jf);
        jf.setLayout(new BoxLayout(jf.getContentPane(),BoxLayout.Y_AXIS));
        //主界面窗口元素.
        jf.setJMenuBar(menuBar(jf));
        addComponents(jf,modeChoosePanel(),pathGetPanel(jf),v2iPanel(),i2vPanel(),processBar());
        //显示窗口.
        jf.setVisible(true);
    }
    private JMenuBar menuBar(JFrame mainWindow) {
        JMenuBar menuBar = new JMenuBar();
        JMenu settingMenu = new JMenu("设置");
        JMenu moreMenu = new JMenu("更多");
        JMenuItem themeSetting = new JMenuItem("主题");
        themeSetting.addActionListener((e -> {
            JDialog dialog = themeSetting(mainWindow);
            dialog.setVisible(true);
        }));
        JMenuItem help = new JMenuItem("帮助");
        JMenuItem about = new JMenuItem("关于");
        //把组件加入容器中.
        settingMenu.add(themeSetting);
        addComponents(moreMenu,help,about);
        addComponents(menuBar,settingMenu,moreMenu);
        return menuBar;
    }
    private JDialog themeSetting(JFrame mainWindow) {
        JDialog dialog = new JDialog(mainWindow,"主题",true);
        dialog.setLayout(new BoxLayout(dialog.getContentPane(),BoxLayout.Y_AXIS));
        dialog.setSize(140,280);
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(mainWindow);
        dialog.setIconImage(icon);
        dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        //窗体组件的绘制.
        JLabel textLabel = new JLabel("主题选择:");
        JRadioButton lightMode = new JRadioButton("Light");
        lightMode.addActionListener(e -> themeModeSet[0] = 0);
        JRadioButton darkMode = new JRadioButton("Dark");
        darkMode.addActionListener(e -> themeModeSet[0] = 1);
        JRadioButton intelliJMode = new JRadioButton("IntelliJ");
        intelliJMode.addActionListener(e -> themeModeSet[0] = 2);
        JRadioButton darculaMode = new JRadioButton("Darcula");
        darculaMode.addActionListener(e -> themeModeSet[0] = 3);
        JRadioButton macLightMode = new JRadioButton("Mac-Light");
        macLightMode.addActionListener(e -> themeModeSet[0] = 4);
        JRadioButton macDarkMode = new JRadioButton("Mac-Dark");
        macDarkMode.addActionListener(e -> themeModeSet[0] = 5);
        switch (themeModeSet[0]) {
            case 0 -> lightMode.setSelected(true);
            case 1 -> darkMode.setSelected(true);
            case 2 -> intelliJMode.setSelected(true);
            case 3 -> darculaMode.setSelected(true);
            case 4 -> macLightMode.setSelected(true);
            case 5 -> macDarkMode.setSelected(true);
        }
        ButtonGroup buttons = new ButtonGroup();
        JButton applyButton = new JButton("应用");
        JLabel noticeLabel = new JLabel("<html><body><br/>注:点击\"应用\"按钮后,<br/>此界面重新打开生效,<br/>主界面立即生效.<body></html>");
        applyButton.addActionListener(e -> {
            switch (themeModeSet[0]) {
                case 0 -> changeLook(light, mainWindow);
                case 1 -> changeLook(dark, mainWindow);
                case 2 -> changeLook(intelliJ, mainWindow);
                case 3 -> changeLook(darcula, mainWindow);
                case 4 -> changeLook(macLight, mainWindow);
                case 5 -> changeLook(macDark, mainWindow);
            }
        });
        //把组件加入容器中.
        addButtons(buttons,lightMode,darkMode,intelliJMode,darculaMode,macLightMode,macDarkMode);
        addComponents(dialog,textLabel,lightMode,darkMode,intelliJMode,darculaMode,macLightMode,macDarkMode,applyButton,noticeLabel);
        return dialog;
    }
    private JPanel modeChoosePanel() {
        JPanel modeChoosePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel textLabel = new JLabel("功能选择:");
        JRadioButton video2image = new JRadioButton("提取视频文件所有帧");
        video2image.setSelected(true);
        video2image.addActionListener(e -> modeSet = 0);
        JRadioButton image2video = new JRadioButton("将文件夹内所有图片合成为视频");
        image2video.addActionListener(e -> modeSet = 1);
        ButtonGroup optionButtons = new ButtonGroup();
        //把组件加入容器中.
        addButtons(optionButtons,video2image,image2video);
        addComponents(modeChoosePanel,textLabel,video2image,image2video);
        return modeChoosePanel;
    }
    private JPanel pathGetPanel(JFrame mainWindow) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel textLabel1 = new JLabel("选择的文件(视频提取帧)或文件夹(用帧合成视频)路径:");
        JTextField pathText1 = new JTextField(40);
        JButton openChooser1 = new JButton("...");
        openChooser1.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            chooser.setMultiSelectionEnabled(false);
            if (chooser.showOpenDialog(mainWindow) == JFileChooser.APPROVE_OPTION) {
                selectedFile = chooser.getSelectedFile();
                pathText1.setText(selectedFile.getPath());
            }
        });
        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel textLabel2 = new JLabel("导出文件的路径:");
        JTextField pathText2 = new JTextField(55);
        JButton openChooser2 = new JButton("...");
        openChooser2.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setMultiSelectionEnabled(false);
            if (chooser.showOpenDialog(mainWindow) == JFileChooser.APPROVE_OPTION) {
                outPath = chooser.getSelectedFile().getPath();
                pathText2.setText(outPath);
            }
        });
        //把组件加入容器中.
        addComponents(panel1,textLabel1,pathText1,openChooser1);
        addComponents(panel2,textLabel2,pathText2,openChooser2);
        addComponents(panel,panel1,panel2);
        return panel;
    }
    private JPanel v2iPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel textLabel = new JLabel("对于提取帧的输出:   扩展名格式:");
        JComboBox<String> extensionSet = new JComboBox<>(new String[]{"png","jpg"});
        extensionSet.addActionListener(e -> selectedExtension = (String) extensionSet.getSelectedItem());
        addComponents(panel,textLabel,extensionSet);
        return panel;
    }
    private JPanel i2vPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel textLabel1 = new JLabel("对于生成视频的输出:   文件名:");
        JTextField fileNameText = new JTextField(20);
        fileNameText.setText("新的视频");
        JLabel textLabel2 = new JLabel("帧率:");
        JTextField fpsText = new JTextField(5);
        fpsText.setText("24");
        JLabel textLabel3 = new JLabel("fps");
        JButton applyButton = new JButton("保存");
        applyButton.addActionListener(e -> {
            selectedName = fileNameText.getText();
            selectedFps = Integer.parseInt(fpsText.getText());
        });
        addComponents(panel,textLabel1,fileNameText,textLabel2,fpsText,textLabel3,applyButton);
        return panel;
    }
    private JPanel processBar() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JProgressBar progressBar = new JProgressBar(0,100);
        JLabel noticeLabel = new JLabel("等待任务...");
        JButton applyButton = new JButton("导出");
        applyButton.addActionListener(e -> {
            noticeLabel.setText("任务执行中...");
            CV cv = new CV(selectedFile,outPath,progressBar,noticeLabel);
            if (modeSet == 0) {
                //此处原因见第13行的注释.
                final int[] status = new int[1];
                Thread v2iThread = new Thread(() -> status[0] = cv.video2image(selectedExtension));
                v2iThread.start();
            } else {
                final int[] status = new int[1];
                Thread i2vThread = new Thread(() -> status[0] = cv.image2video(selectedName,selectedFps));
                i2vThread.start();
            }
        });
        JLabel textLabel = new JLabel("进度:");
        //把组件加入容器中.
        addComponents(panel,applyButton,textLabel,progressBar,noticeLabel);
        return panel;
    }
}
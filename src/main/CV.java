package main;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;

import javax.swing.*;
import java.io.File;

public class CV {
    private final File inputFile;
    private final String outPath;
    private final JProgressBar bar;
    private final JLabel noticeLabel;
    /**
     * 引用openCV的构造方法.在此处加载openCV库.
     * @param inputFile 传入的文件.
     * @param outPath 保存文件的路径.(e.g. D:\\folder)
     * @param progressBar 连接的进度条.
     * @param noticeLabel 连接的提示文本.
     */
    public CV(File inputFile,String outPath,JProgressBar progressBar,JLabel noticeLabel) {
        System.load("D:\\Development\\库\\opencv\\build\\java\\x64\\opencv_java470.dll");
        this.inputFile = inputFile;
        this.outPath = outPath;
        bar = progressBar;
        this.noticeLabel = noticeLabel;
    }
    /**
     * 使用OpenCV把视频中所有的帧全部提取为图片.
     * 返回值含义:
     * 0.正常运行.
     * 1.传入的文件不是一个文件.
     * 2.错误:VideoCapture无法打开视频.
     * @param imageExtension 保存图片的后缀名(e.g. png).
     * @return 如果方法产生错误,将通过返回值告诉引用方法的地方.
     */
    public int video2image(String imageExtension) {
        if (inputFile.isFile()) {
            VideoCapture videoCapture = new VideoCapture(inputFile.getPath());
            if (!videoCapture.isOpened()) {
                return 2;
            }
            int frameTotal = (int) videoCapture.get(Videoio.CAP_PROP_FRAME_COUNT);
            Mat frame = new Mat();
            int frameCount = 1;
            while (videoCapture.read(frame)) {
                Imgcodecs.imwrite(outPath + "/" + frameCount + "." + imageExtension,frame);
                updateProgressBar(frameCount / frameTotal * 100);
                frameCount++;
            }
            videoCapture.release();
            System.out.println("执行完成!");

            return 0;
        } else {
            return 1;
        }
    }
    /**
     * 使用OpenCV将图片转换成视频(只支持.mp4).
     * 返回值含义:
     * 0.正常运行.
     * 1.传入的不是一个目录.
     * 2.目录里面没有文件.
     * @param name 视频的文件名.
     * @param fps 视频的帧率.
     * @return 如果方法产生错误,将通过返回值告诉引用方法的地方.
     */
    public int image2video(String name,int fps) {
        if (inputFile.isDirectory()) {
            File[] img = new File[inputFile.listFiles().length];
            for (int i = 1; i - 1 < img.length; i++) {
                img[i - 1] = new File(inputFile.getPath() + "/" + i + ".png");
            }
            if (img.length > 0) {
                Mat example = Imgcodecs.imread(img[0].getPath());
                VideoWriter videoWriter = new VideoWriter(outPath + "/" + name + ".mp4",
                        VideoWriter.fourcc('m','p','4','v'),fps,new Size(example.width(),example.height()),true);
                Mat frame;
                int total = img.length;
                for (int i = 1;i - 1 < total;i++) {
                    frame = Imgcodecs.imread(img[i - 1].getAbsolutePath());
                    if (frame.empty()) {
                        System.out.println("错误,图片为空!:" + img[i - 1].getPath());
                    }
                    videoWriter.write(frame);
                    updateProgressBar(i / total * 100);
                }
                videoWriter.release();
                System.out.println("执行完成!");
                return 0;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }
    /**
     * 通过此方法更新进度条.
     * @param index 进度条的值(0 - 100).
     */
    private void updateProgressBar(int index) {
        SwingUtilities.invokeLater(() -> {
            bar.setValue(index);
            bar.repaint();
        });
    }
    private void over() {
        SwingUtilities.invokeLater(() -> {
            noticeLabel.setText("等待任务...");
            noticeLabel.repaint();
        });
    }
}
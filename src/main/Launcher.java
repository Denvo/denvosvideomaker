package main;

import UI.MainUI;

/**
 * 程序DenvosVideoMaker的启动类.
 * 关于更多信息,请参阅:
 * @author Denvo Zonis
 * @version 0.0.1
 */

public class Launcher { public static void main(String[] args) {new MainUI();}}